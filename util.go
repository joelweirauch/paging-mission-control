package main

import (
  "time"
  "strconv"
)

func check(e error) {
  if e != nil {
    panic(e)
  }
}

func parseTimestamp(raw string) time.Time {
  parsedTS, err := time.Parse("20060102 15:04:05", raw)
  check(err)

  return parsedTS
}

func stoi(s string) int {
  i, _ := strconv.Atoi(s)

  return i
}

func stof(s string) float64 {
  f, _ := strconv.ParseFloat(s, 32)

  return f
}
