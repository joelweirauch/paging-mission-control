package main

import (
  "database/sql"
  "time"
  "fmt"
)

func createDatabase(db *sql.DB) {
  stmt := `
create table telemetry(
  timestamp integer not null,
  satellite_id integer not null,
  red_high_limit integer not null,
  yellow_high_limit integer not null,
  yellow_low_limit integer not null,
  red_low_limit integer not null,
  raw_value real not null,
  component text
);

create table alerts(
  satellite_id integer not null,
  alert_start integer not null,
  component text
);
  `
  _, err := db.Exec(stmt)
  check(err)
}

func saveTelemetry(db *sql.DB, t Telemetry) {
  tx, err := db.Begin()
  check(err)

  sql := `
insert into telemetry (
  timestamp, 
  satellite_id, 
  red_high_limit, 
  yellow_high_limit, 
  yellow_low_limit, 
  red_low_limit, 
  raw_value, 
  component
) values(?, ?, ?, ?, ?, ?, ?, ?)
  `
  stmt, err := tx.Prepare(sql)
  check(err)

  defer stmt.Close()

  _, err = stmt.Exec(
    t.Timestamp, 
    t.SatelliteId, 
    t.RedHighLimit, 
    t.YellowHighLimit, 
    t.YellowLowLimit, 
    t.RedLowLimit, 
    t.RawValue, 
    t.Component,
  )
  check(err)

  err = tx.Commit()
  check(err)
}

func checkActiveAlert(db *sql.DB, t Telemetry) bool {
  var count int

  checkSQL := `SELECT COUNT(*) FROM alerts WHERE satellite_id=? AND component=?`

  stmt, err := db.Prepare(checkSQL)
  check(err)
  defer stmt.Close()

  countRow := stmt.QueryRow(t.SatelliteId, t.Component)
  err = countRow.Scan(&count)
  check(err)

  if (count >= 1) {
    return true
  }

  return false
}

func setAlert(db *sql.DB, t Telemetry, ts string) {
  tx, err := db.Begin()
  check(err)

  sql := `
insert into alerts (
  satellite_id, 
  component,
  alert_start
) values(?, ?, ?)
  `
  stmt, err := tx.Prepare(sql)
  check(err)

  defer stmt.Close()

  _, err = stmt.Exec(
    t.SatelliteId, 
    t.Component,
    ts,
  )
  check(err)

  err = tx.Commit()
  check(err)
}

func clearAlert(db *sql.DB, t Telemetry) {
  tx, err := db.Begin()
  check(err)

  sql := `DELETE FROM alerts WHERE satellite_id=? AND component=?`
  stmt, err := tx.Prepare(sql)
  check(err)

  defer stmt.Close()

  _, err = stmt.Exec(
    t.SatelliteId, 
    t.Component,
  )
  check(err)

  err = tx.Commit()
  check(err)
}

func checkHighTemperature(db *sql.DB, t Telemetry) string {
  var count int

  lowerTS := t.Timestamp.Add(-time.Minute * 5)

  highTempSQL := `
SELECT 
  %s 
FROM 
  telemetry 
WHERE 
  satellite_id=? AND component='TSTAT' AND raw_value > red_high_limit AND timestamp BETWEEN ? AND ?
  `

  countStmt, err := db.Prepare(fmt.Sprintf(highTempSQL, "COUNT(*)"))
  check(err)
  defer countStmt.Close()

  countRow := countStmt.QueryRow(t.SatelliteId, lowerTS, t.Timestamp)

  err = countRow.Scan(&count)
  check(err)

  if (count >= 3) {
    stmt, err := db.Prepare(fmt.Sprintf(highTempSQL, "timestamp"))
    check(err)
    defer stmt.Close()

    rows, err := stmt.Query(t.SatelliteId, lowerTS, t.Timestamp)
    check(err)
    defer rows.Close()

    var timestamp string

    for (rows.Next()) {
      err = rows.Scan(&timestamp)
      check(err)

      if (checkActiveAlert(db, t)) {
        return ""
      } else {
        return timestamp
      }
    }
  }

  // We could check first to see if there's an alert before trying to clear, but this will only
  // do a single database operation vs multiple to check and then delete.
  clearAlert(db, t)

  return ""
}

func checkLowBattery(db *sql.DB, t Telemetry) string {
  var count int

  lowerTS := t.Timestamp.Add(-time.Minute * 5)

  lowBatterySQL := `
SELECT 
  %s 
FROM 
  telemetry 
WHERE 
  satellite_id=? AND component='BATT' AND raw_value < red_low_limit AND timestamp BETWEEN ? AND ?
  `

  countStmt, err := db.Prepare(fmt.Sprintf(lowBatterySQL, "COUNT(*)"))
  check(err)
  defer countStmt.Close()

  countRow := countStmt.QueryRow(t.SatelliteId, lowerTS, t.Timestamp)

  err = countRow.Scan(&count)
  check(err)

  if (count >= 3) {
    stmt, err := db.Prepare(fmt.Sprintf(lowBatterySQL, "timestamp"))
    check(err)
    defer stmt.Close()

    rows, err := stmt.Query(t.SatelliteId, lowerTS, t.Timestamp)
    check(err)
    defer rows.Close()

    var timestamp string

    for (rows.Next()) {
      err = rows.Scan(&timestamp)
      check(err)

      if (checkActiveAlert(db, t)) {
        return ""
      } else {
        return timestamp
      }
    }
  }

  // We could check first to see if there's an alert before trying to clear, but this will only
  // do a single database operation vs multiple to check and then delete.
  clearAlert(db, t)

  return ""
}
