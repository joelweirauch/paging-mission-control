package main

import (
  "fmt"
  "bufio"
  "time"
  "strings"
  "os"
  "database/sql"
  "encoding/json"
  _ "github.com/mattn/go-sqlite3"
)

type Telemetry struct {
  Timestamp       time.Time
  SatelliteId     int
  RedHighLimit    int
  YellowHighLimit int
  YellowLowLimit  int
  RedLowLimit     int
  RawValue        float64
  Component       string
}

type Alert struct {
  SatelliteId int    `json:"satelliteId"`
  Severity    string `json:"severity"`
  Component   string `json:"component"`
  Timestamp   string `json:"timestamp"`
}



func newTelemetry(data []string) Telemetry {
  t := Telemetry {
    Timestamp:       parseTimestamp(data[0]),
    SatelliteId:     stoi(data[1]),
    RedHighLimit:    stoi(data[2]),
    YellowHighLimit: stoi(data[3]),
    YellowLowLimit:  stoi(data[4]),
    RedLowLimit:     stoi(data[5]),
    RawValue:        stof(data[6]),
    Component:       data[7],
  }

  return t
}


func main() {
  alerts := []Alert{}

  os.Remove("./telemetry.db")

  db, err := sql.Open("sqlite3", "./telemetry.db")
  check(err)
  
  defer db.Close()

  createDatabase(db)

  args := os.Args[1:]

  if (len(args) < 1) {
    panic("You must specify an input file as the first argument.")
  }

  file, err := os.Open(args[0])
  check(err)

  defer file.Close()

  scanner := bufio.NewScanner(file)

  scanner.Split(bufio.ScanLines)

  for scanner.Scan() {
    data := strings.Split(scanner.Text(), "|")

    t := newTelemetry(data)
    saveTelemetry(db, t)

    if (t.Component == "TSTAT") {
      ts := checkHighTemperature(db, t)
      if (ts != "") {
         setAlert(db, t, ts)
         alerts = append(
           alerts,
           Alert{
             SatelliteId: t.SatelliteId,
             Severity: "RED HIGH",
             Component: "TSTAT",
             Timestamp: ts,  // We want to set the alert to the first occurrence of the condition
           },
         )
      }
    }

    if (t.Component == "BATT") {
      ts := checkLowBattery(db, t)
      if (ts != "") {
         setAlert(db, t, ts)
         alerts = append(
           alerts,
           Alert{
             SatelliteId: t.SatelliteId,
             Severity: "RED LOW",
             Component: "BATT",
             Timestamp: ts,  // We want to set the alert to the first occurrence of the condition
           },
         )
      }
    }
  }

  alertsJson, err := json.MarshalIndent(alerts, "", "    ")
  check(err)

  fmt.Println(string(alertsJson))
}
